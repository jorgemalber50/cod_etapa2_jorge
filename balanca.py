from pilha import stack

def is_balanced(paren_str):
    balanca = stack() ##criar pilha vazia
    is_balanced=True  ## flag 
    index = 0 ## indice
    while index < len(paren_str) and is_balanced :
        paren=paren_str[index]
        if paren in "([{":
            balanca.push(paren)
        else:
            if balanca.is_empty():
                is_balanced=False
            else:
                topo=balanca.pop() ##.pop() sem argumentos retornar o item -1 ou seja indice do ultimo item inserido
                if not is_match(topo,paren):
                    is_balanced=False
        
        index += 1
    if balanca.is_empty and is_balanced:
        return True
    else:
        return False
    
def is_match(p1,p2):
    if   p1 == "(" and p2 == ")":
        return True
    elif p1 == "[" and p2 == "]":
        return True
    elif p1 == "{" and p2 == "}":
        return True
    else:
        return False 

#Testes
teste1 = "(( ))" # true
teste2 = "{{}}()" # true
teste3 = "()()" #true
teste4 = "([)]" #false 
teste5 = "{[}(("#false
teste6= "    "#false, deveria criar um 'key error'
teste7= "(something)" #false, deveria criar um 'key error' 

print(
is_balanced(teste1),"\n",
is_balanced(teste2),"\n",
is_balanced(teste3),"\n",
is_balanced(teste4),"\n",
is_balanced(teste5),"\n",
is_balanced(teste6),"\n",
is_balanced(teste7),"\n"
)