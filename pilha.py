class stack():
    def __init__(self):
        ## o construtor usa uma lista vazia
        self.items=[] 

    ## colocar um elemento na pilha ##

    def push(self,item):
        self.items.append(item)

    ## tirar o elemento da pilha##
    def pop(self):
        if not self.is_empty():
            return self.items.pop()
        else :
            raise KeyError ('Não há elementos nessa pilha')
    
    ## verificar se a pilha está vazia##
    
    def is_empty(self):
        return self.items == []

    ##esvaziar pilha##

    def empty(self):
        self.items = []

    ## obter o item do topo da pilha##


    def peek(self): 
        if not self.is_empty(): ## se a pilha não estiver vazia
            ## retornar o primeiro elemento a entrar na pilha (último da lista) 
            ## por isso o indexador negativo 
            return self.items[-1] 
        else:
            raise KeyError ('Não há elementos nessa pilha')
        
                                  
    ## obter os elementos contidos na pilha##                              
    
    def elements(self):
    
        if not self.is_empty():
            return self.items
        else:
            raise KeyError ('Não há elementos nessa pilha')
        
    